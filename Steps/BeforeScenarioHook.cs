﻿using TobaniaTesting.WrapperFactory;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace TobaniaTesting.Steps
{
    [Binding]
    class BeforeScenarioHook
    {
        [BeforeScenario]
        public void InitializeBrowser()
        {
            WebDriverFactory.InitBrowser(ConfigurationManager.AppSettings["BROWSER"]);
            WebDriverFactory.LoadApplication(ConfigurationManager.AppSettings["URL"]);
        }
    }
}
