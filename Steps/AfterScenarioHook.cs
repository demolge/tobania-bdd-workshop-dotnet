﻿using TobaniaTesting.WrapperFactory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace TobaniaTesting.Steps
{
    [Binding]
    class AfterScenarioHook
    {
        [AfterScenario]
        public void CloseBrowser()
        {
            WebDriverFactory.CloseAllDrivers();
        }
    }
}
