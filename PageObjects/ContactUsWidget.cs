﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TobaniaTesting.PageObjects
{
    class ContactUsWidget
    {
        [FindsBy(How = How.Id, Using = "uniform-id_contact")]
        private IWebElement contactUsWidget { get; set; }

        public bool IsContactWidgetDisplayed()
        {
            return contactUsWidget.Displayed;
        }
    }
}
