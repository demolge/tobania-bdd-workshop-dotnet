﻿using TobaniaTesting.WrapperFactory;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TobaniaTesting.PageObjects
{
    class Page
    {
        private static T GetPage<T>() where T : new()
        {
            var page = new T();
            PageFactory.InitElements(WebDriverFactory.Driver, page);
            return page;
        }

        public static HomePage HomePage
        {
            get { return GetPage<HomePage>(); }
        }

        public static ContactUsWidget ContactUsWidget
        {
            get { return GetPage<ContactUsWidget>(); }
        }
    }
}
